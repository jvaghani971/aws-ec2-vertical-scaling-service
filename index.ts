import express, { Request, Response } from "express";
import bodyParser from "body-parser";
import * as dotenv from "dotenv";
dotenv.config();
import {
  describeInstanceStatus,
  modifyInstanceType,
  isValidInstanceType,
} from "./awsUtility";

const app = express();

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());

app.get("/instance/state/:instanceId", async (req: Request, res: Response) => {
  try {
    const result = await describeInstanceStatus(req.params.instanceId);
    if(result.Reservations && result.Reservations.length === 1 && result.Reservations[0].Instances && result.Reservations[0].Instances.length === 1){
      res.json({
        status: 1,
        data: (result.Reservations[0].Instances[0].State) as any,
      });
      return
    }
    throw new Error("No such instance found!")
  } catch (err) {
    res.status(400).json({
      status: 0,
      message: err.message || "something went wrong!",
    });
  }
});

app.post("/instance/modify", async (req: Request, res: Response) => {
  try {
    console.log(req.body);

    if (!isValidInstanceType(req.body.instanceType)) {
      throw new Error("Invalid instance type!");
    }
    const result = await modifyInstanceType(
      req.body.instanceId,
      req.body.instanceType
    );
    res.json({
      status: 1,
      data: result,
    });
  } catch (err) {
    res.status(400).json({
      status: 0,
      message: err.message || "something went wrong!",
    });
  }
});

app.listen(process.env.port, () => {
  console.log(`App started listening on port: ${process.env.port}`);
});
