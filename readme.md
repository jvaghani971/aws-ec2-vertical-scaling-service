# Aws EC2 Vertical Scalling

This server provides rest api to check EC2 instance status and to vertical scale the EC2 instance.

## Installation

Use the package manager [yarn](https://classic.yarnpkg.com/en/) to install dependencies.

```npm
yarn install
```

## Usage

Run the server in development mode.

```npm
npm run dev
```

Run the server in production.

```npm
npm run production
```


## Postman Collection

Import "./aws vertical scalling.postman_collection.json" file in the postman 