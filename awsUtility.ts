import { EC2 } from "aws-sdk";

const a = new EC2({
  accessKeyId: process.env.aws_accessKeyId,
  secretAccessKey: process.env.aws_secretAccessKey,
  region: process.env.aws_region,
});

export type InstanceTypes =
  | "a1.medium"
  | "a1.large"
  | "a1.xlarge"
  | "a1.2xlarge"
  | "a1.4xlarge"
  | "a1.metal"
  | "m4.large"
  | "m4.xlarge"
  | "m4.2xlarge"
  | "m4.4xlarge"
  | "m4.10xlarge"
  | "m4.16xlarge"
  | "m5.large"
  | "m5.xlarge"
  | "m5.2xlarge"
  | "m5.4xlarge"
  | "m5.8xlarge"
  | "m5.12xlarge"
  | "m5.16xlarge"
  | "m5.24xlarge"
  | "m5.metal"
  | "m5a.large"
  | "m5a.xlarge"
  | "m5a.2xlarge"
  | "m5a.4xlarge"
  | "m5a.8xlarge"
  | "m5a.12xlarge"
  | "m5a.16xlarge"
  | "m5a.24xlarge"
  | "m5ad.large"
  | "m5ad.xlarge"
  | "m5ad.2xlarge"
  | "m5ad.4xlarge"
  | "m5ad.8xlarge"
  | "m5ad.12xlarge"
  | "m5ad.16xlarge"
  | "m5ad.24xlarge"
  | "m5d.large"
  | "m5d.xlarge"
  | "m5d.2xlarge"
  | "m5d.4xlarge"
  | "m5d.8xlarge"
  | "m5d.12xlarge"
  | "m5d.16xlarge"
  | "m5d.24xlarge"
  | "m5d.metal"
  | "m5dn.large"
  | "m5dn.xlarge"
  | "m5dn.2xlarge"
  | "m5dn.4xlarge"
  | "m5dn.8xlarge"
  | "m5dn.12xlarge"
  | "m5dn.16xlarge"
  | "m5dn.24xlarge"
  | "m5n.large"
  | "m5n.xlarge"
  | "m5n.2xlarge"
  | "m5n.4xlarge"
  | "m5n.8xlarge"
  | "m5n.12xlarge"
  | "m5n.16xlarge"
  | "m5n.24xlarge"
  | "m6g.medium"
  | "m6g.large"
  | "m6g.xlarge"
  | "m6g.2xlarge"
  | "m6g.4xlarge"
  | "m6g.8xlarge"
  | "m6g.12xlarge"
  | "m6g.16xlarge"
  | "m6g.metal"
  | "t2.nano"
  | "t2.micro"
  | "t2.small"
  | "t2.medium"
  | "t2.large"
  | "t2.xlarge"
  | "t2.2xlarge"
  | "t3.nano"
  | "t3.micro"
  | "t3.small"
  | "t3.medium"
  | "t3.large"
  | "t3.xlarge"
  | "t3.2xlarge"
  | "t3a.nano"
  | "t3a.micro"
  | "t3a.small"
  | "t3a.medium"
  | "t3a.large"
  | "t3a.xlarge"
  | "t3a.2xlarge";

export enum InstanceStateCode {
  pending = 0,
  running = 16,
  shuttingDown = 32,
  terminated = 48,
  stopping = 64,
  stopped = 80,
}

export const isValidInstanceType = (type: string) => {
  const validTypes = [
    "a1.medium",
    "a1.large",
    "a1.xlarge",
    "a1.2xlarge",
    "a1.4xlarge",
    "a1.metal",
    "m4.large",
    "m4.xlarge",
    "m4.2xlarge",
    "m4.4xlarge",
    "m4.10xlarge",
    "m4.16xlarge",
    "m5.large",
    "m5.xlarge",
    "m5.2xlarge",
    "m5.4xlarge",
    "m5.8xlarge",
    "m5.12xlarge",
    "m5.16xlarge",
    "m5.24xlarge",
    "m5.metal",
    "m5a.large",
    "m5a.xlarge",
    "m5a.2xlarge",
    "m5a.4xlarge",
    "m5a.8xlarge",
    "m5a.12xlarge",
    "m5a.16xlarge",
    "m5a.24xlarge",
    "m5ad.large",
    "m5ad.xlarge",
    "m5ad.2xlarge",
    "m5ad.4xlarge",
    "m5ad.8xlarge",
    "m5ad.12xlarge",
    "m5ad.16xlarge",
    "m5ad.24xlarge",
    "m5d.large",
    "m5d.xlarge",
    "m5d.2xlarge",
    "m5d.4xlarge",
    "m5d.8xlarge",
    "m5d.12xlarge",
    "m5d.16xlarge",
    "m5d.24xlarge",
    "m5d.metal",
    "m5dn.large",
    "m5dn.xlarge",
    "m5dn.2xlarge",
    "m5dn.4xlarge",
    "m5dn.8xlarge",
    "m5dn.12xlarge",
    "m5dn.16xlarge",
    "m5dn.24xlarge",
    "m5n.large",
    "m5n.xlarge",
    "m5n.2xlarge",
    "m5n.4xlarge",
    "m5n.8xlarge",
    "m5n.12xlarge",
    "m5n.16xlarge",
    "m5n.24xlarge",
    "m6g.medium",
    "m6g.large",
    "m6g.xlarge",
    "m6g.2xlarge",
    "m6g.4xlarge",
    "m6g.8xlarge",
    "m6g.12xlarge",
    "m6g.16xlarge",
    "m6g.metal",
    "t2.nano",
    "t2.micro",
    "t2.small",
    "t2.medium",
    "t2.large",
    "t2.xlarge",
    "t2.2xlarge",
    "t3.nano",
    "t3.micro",
    "t3.small",
    "t3.medium",
    "t3.large",
    "t3.xlarge",
    "t3.2xlarge",
    "t3a.nano",
    "t3a.micro",
    "t3a.small",
    "t3a.medium",
    "t3a.large",
    "t3a.xlarge",
    "t3a.2xlarge",
  ];
  return validTypes.includes(type);
};

export const startInstance = (
  id: EC2.InstanceId
): Promise<EC2.StartInstancesResult> =>
  new Promise((resolve, reject) => {
    a.startInstances(
      {
        InstanceIds: [id],
      },
      (err, data) => {
        if (err) {
          reject(err);
        }
        resolve(data);
      }
    );
  });

export const stopInstance = (
  id: EC2.InstanceId
): Promise<EC2.StopInstancesResult> =>
  new Promise((resolve, reject) => {
    a.stopInstances(
      {
        InstanceIds: [id],
      },
      (err, data) => {
        if (err) {
          reject(err);
        }
        resolve(data);
      }
    );
  });

export const describeInstanceStatus = (
  id: EC2.InstanceId
): Promise<EC2.DescribeInstancesResult> =>
  new Promise((resolve, reject) => {
    a.describeInstances(
      {
        InstanceIds: [id],
      },
      (err, data) => {
        if (err) {
          reject(err);
        }
        resolve(data);
      }
    );
  });

export const describeInstanceTypeOfferings = (): Promise<
  EC2.DescribeInstanceTypeOfferingsResult
> =>
  new Promise((resolve, reject) => {
    a.describeInstanceTypeOfferings({}, (err, data) => {
      if (err) {
        reject(err);
      }
      resolve(data);
    });
  });

export const changeInstanceType = (
  id: EC2.InstanceId,
  type: EC2.InstanceType
): Promise<EC2.ModifyInstanceAttributeRequest> =>
  new Promise((resolve, reject) => {
    a.modifyInstanceAttribute(
      {
        InstanceId: id,
        InstanceType: {
          Value: type,
        },
      },
      (err, data) => {
        if (err) {
          reject(err);
        }
        resolve();
      }
    );
  });

const sleep = (seconds: number) =>
  new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, seconds * 1000);
  });

export const modifyInstanceType = async (
  InstanceId: string,
  type: InstanceTypes
) => {
  // check current status
  const status = await describeInstanceStatus(InstanceId);
  if (status.Reservations && status.Reservations.length === 1 &&  status.Reservations[0].Instances && status.Reservations[0].Instances[0].InstanceType === type) {
    return "Instance type is already set.";
  }

  if (
    status.Reservations && status.Reservations.length === 1 &&  status.Reservations[0].Instances &&
    status.Reservations[0].Instances[0].State?.Code !==
    InstanceStateCode.stopped
  ) {
    const data = await stopInstance(InstanceId);
    if (
      data.StoppingInstances &&
      data.StoppingInstances.length === 1 &&
      data.StoppingInstances[0].CurrentState
    ) {
      console.log("stopping instance....");
    }

    while (true) {
      const status = await describeInstanceStatus(InstanceId);
      if (
        status.Reservations &&
        status.Reservations.length === 1 &&
        status.Reservations[0].Instances &&
        status.Reservations[0].Instances.length === 1 &&
        status.Reservations[0].Instances[0].State
      ) {
        process.stdout.clearLine(0);
        process.stdout.cursorTo(0);
        process.stdout.write(
          "current status:-- " + status.Reservations[0].Instances[0].State.Name
        );

        if (
          status.Reservations[0].Instances[0].State.Code ===
          InstanceStateCode.stopped
        ) {
          process.stdout.clearLine(0);
          process.stdout.cursorTo(0);
          process.stdout.write("instance stoped.\n");
          break;
        }
      } else {
        console.log("Something went wrong: terminating:----", status);
        throw new Error("Something went wrong");
      }
      await sleep(5);
    }
  }

  //    const types = await describeInstanceTypeOfferings()

  //    if(!types.InstanceTypeOfferings || types.InstanceTypeOfferings.length <= 1){
  //        console.log("No instance type offerings awailable")
  //        exitProcess()
  //    }
  //     console.log("choose instance type")
  //     types.InstanceTypeOfferings?.map((offering,index)=>{
  //         console.log(`${index+1}) ${offering.InstanceType}`)
  //     })

  await changeInstanceType(InstanceId, type);

  const startResult = await startInstance(InstanceId);
  if (
    startResult.StartingInstances &&
    startResult.StartingInstances.length === 1 &&
    startResult.StartingInstances[0].CurrentState
  ) {
    console.log("starting instance....");
  }

  while (true) {
    const status = await describeInstanceStatus(InstanceId);
    if (
      status.Reservations &&
      status.Reservations.length === 1 &&
      status.Reservations[0].Instances &&
      status.Reservations[0].Instances.length === 1 &&
      status.Reservations[0].Instances[0].State
    ) {
      process.stdout.clearLine(0);
      process.stdout.cursorTo(0);
      process.stdout.write(
        "current status:---" + status.Reservations[0].Instances[0].State.Name
      );

      if (
        status.Reservations[0].Instances[0].State.Code ===
        InstanceStateCode.running
      ) {
        process.stdout.clearLine(0);
        process.stdout.cursorTo(0);
        process.stdout.write("Instance started.\n");
        break;
      }
    } else {
      console.log("Something went wrong: terminating:----", status);
      throw new Error("Something went wrong!");
    }
    await sleep(5);
  }

  return "Instance type is updated and running!";
};
